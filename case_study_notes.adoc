= Djupvitr - Machine learning in computational cloud's resource planning

== Related technologies/tools

=== ETSI

==== Network Function Virtualization (NFV)

#TODO#


==== Management and Orchestration (MANO)

#TODO#

===== Open Source MANO (OSM)

Main implementation of MANO specification.


==== Experimental Networked Intelligence (ENI)

#TODO#


=== Custom MANO implementations

==== Sonata MANO: 
Git: https://github.com/sonata-nfv/son-mano-framework

==== OpenBaton: 
Web: http://openbaton.github.io/index.html

Git: https://github.com/openbaton

Available VIM drivers:

* _openstack_ - for OpenStack
* _amazon_ - for Amazon AWS
* _docker_ - for Docker/Docker Swarm
* _test_ - for testing of internal business logic of the NFVO (not applicable for our case)

==== OpenDaylight: 
Web: https://www.opendaylight.org/

==== RiftIO: 
Web: https://riftio.com/


=== Mininet & OpenNet & ns-3

Some well known technologies for emulation of network (wireless network with OpenNet).
Feasible as base for other, higher-level tooling (see NIEP)


=== NIEP (mininet + NFV): 
Git: https://github.com/ViniGarcia/NIEP

Emulation framework based on minimet/docker (dockernet) and NFV specification. No MANO.
Artifact of related atricle.


=== Blazar
Web: https://wiki.openstack.org/wiki/Blazar

Git: https://github.com/openstack/blazar

Resource reservation service that allows leasing specific type/amout of resources for specific time period (immedietely or in future).
Uses _Nova scheduler_ for host reservation.
Lease consist of reservation of physical/virtual resource type and resource amount, start time and end time of lease. Returned resevation ID
is later used by created server as a hint for scheduler (server won't start if reservation is invalid).

*Current stable version:* 2.0.0 (30 VIII 2018) - https://github.com/openstack/blazar/archive/2.0.0.tar.gz

.Notable stable release notes
* Added time margin for cleaning up resources between end of lease and start of next one
* Minor API fixes and response changes


*Newest version:* 2.0.0-27 (20 XI 2018)

.New release specification
* Currently no specs given for next release


*Notable functions (aggregated from all releases):*

* Virtual instance reservation
* Terminate lease at any time
* Monitor state of leased resources


Currently supports only two types of resources: _Compute Host_ (whole host unit) or _Instance_ (seems as virtual instance; *NOT SURE IF IT WORKS*)


Compute hosts have to be marked as possible to be reserved. User can ask for compute hosts reservation based on:

* region
* number of CPU cores
* amount of free RAM
* amount of free disk space
* number of hosts
* extra capabilities


Available modules:

* _blazar-client_ - user interface
* _blazar-api_ - connects user interface with manager
* _blazar-manager_ - manages logic related to leases, reservations and events (needs DB for data storage); uses resource-plugins to work with concrete resource type
* _resource-plugin_ - responsible for concrete resource (communicates with Virtualized Infrastructure Managers (VIMs))

Monitoring is either push-based or polling-based. In case of resource failure Blazar will try to periodically heal started or soon to be started reservations.

.Available resource monitoring plugins:
* _Compute Host Monitor_

*Roadmap/missing:*

* *Virtual instances reservation* - note in current user guide; blueprint on Launchpad and release notes say otherwise (seems to be implemented)
* Virutal instances monitoring plugin - no mentions in user guide


*Self notes:*

* Given energy consumption optimization and limited amount of physical nodes we could use few virtual nodes as mocks for physical ones (drawback is that we would have virtualisation on virtualised hosts)


== Related articles

* _Survey on Dynamic Resource Allocation Strategy in Cloud Computing Environemnt_, N. Krishnaveni, G. Sivakumar
* _Energy Efficient Dynamic Resource Allocation Technique in Mobile Cloud Computing_, Neha Jayant, Gagandeep
* _A large-scale NFV-based emulation platform for smart identifier network_, Li Haifeng, Li Taixin, Zhang Hongke
* _NIEP: NFV Infrastructure Emulation Platform_, Thales Nicolai Tavares
* _Reinforcement Learning for Resource Provisioning in Vehicular Cloud_, Mohammad A. Salahuddin
